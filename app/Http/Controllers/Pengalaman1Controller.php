<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Pengalaman1Controller extends Controller
{
    public function index(){
        return view('pengalaman.Pengalaman1',[
            "header" => "1",
            "judul" => "KOOR SUB BIDANG TARI HMJ TI 2020/2021",
            "konten" => "Saya pernah menjadi koor subbidang tari di HMJ TI 2020/2021."
        ]);
        }   
}
