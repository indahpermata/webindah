<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Pengalaman2Controller extends Controller
{
    public function index(){
        return view('pengalaman.Pengalaman2',[
            "header" => "2",
            "judul" => "KOOR BIDANG 2 HMJ TI 2021/2022",
            "konten" => "Saya saat ini menjabat menjadi koor bidang 2 HMJ TI pada tahun jabatan 2021/2022 yang mengurusi terkait minat dan bakat Mahasiswa Jurusan TI."
        ]);
        }   
}
