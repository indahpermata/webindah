<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Pengalaman3Controller extends Controller
{
    public function index(){
    return view('pengalaman.Pengalaman3',[
        "header" => "3",
        "judul" => "LOMBA VIDEO PENDEK",
        "konten" => "Saya pernah mengikuti perlombaan Video Pendek yang diselenggarakan oleh Forkom Bidikmisi Undiksha dengan meraih juara 3."
    ]);
    }
}
