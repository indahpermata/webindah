<!DOCTYPE html>
<html lang="en">

<head>
  <!-- meta -->
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>{{ $header }}</title>
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Poppins:300,300i,400,400i,500,500i,600,600i,700,700i|Playfair+Display:400,400i,700,700i,900,900i" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="lib/ionicons/css/ionicons.min.css" rel="stylesheet">
  <link href="lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="lib/magnific-popup/magnific-popup.css" rel="stylesheet">
  <link href="lib/hover/hover.min.css" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="css/style.css" rel="stylesheet">

  <!-- Responsive css -->
  <link href="css/responsive.css" rel="stylesheet">

  <!-- Favicon -->
  <link rel="shortcut icon" href="images/favicon.png">

  <!-- Bootstrap 3 & Ajax untuk Modal -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>

<body>

  <!-- navbar -->
  <nav id="main-nav">
    <div class="row">
      <div class="container">
        <div class="logo">
          <a href="/"><img src="images/logo.png" alt="logo"></a>
        </div>
        <div class="responsive"><i data-icon="m" class="ion-navicon-round"></i></div>
        <ul class="nav-menu list-unstyled">
          <li><a href="#header" class="smoothScroll">Home</a></li>
          <li><a href="#about" class="smoothScroll">Biodata</a></li>
          <li><a href="#journal" class="smoothScroll">Pengalaman</a></li>
          <li><a href="#portfolio" class="smoothScroll">Galery</a></li>
        </ul>
      </div>
    </div>
  </nav>
  <!-- navbar -->


  <!-- header -->
  <div id="header" class="home">
    <div class="container">
      <div class="header-content">
        <h1>Saya <span class="typed"></span></h1>
        <div class="box">
          <span></span>
          <span></span>
          <span></span>
        </div>
      </div>
    </div>
  </div>
  <!-- header -->


  <!-- start section about me -->
  <div id="about" class="paddsection">
    <div class="container">
      <div class="row justify-content-between">
        <div class="col-lg-4 ">
          <div class="div-img-bg">
            <div class="about-img">
              <img src="images/me.jpeg" class="img-responsive" alt="me">
                <div class="middle">
                  <div class="text">Indah Permata Aji</div>
                </div>
            </div>
          </div>
        </div>
        <div class="col-lg-7">
          <div class="about-descr">
            <p class="p-heading">
              Hai,
            </p>
            <p class="separator">
              Saya Putu Indah Permata Aji. Saya sekarang seorang mahasiswa di Universitas Pendidikan Ganesha Prodi Pendidikan Teknik Informatika.
              Saya lahir di Singaraja dan hobi saya adalah menari. <br>
              Saya memulai sekolah dasar di SD Negeri 1 Julah 
              dan melanjutkan di SMP Negeri 4 Tejakula. Namun, memasuki sekolah menengah kejuruan saya bersekolah di Singaraja 
              yakni SMK Negeri 3 Singaraja jurusan Multimedia.
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- end section about us -->

<!-- awal skill -->
<div id="services">
    <div class="container">
        <div class="services-carousel owl-theme">
          <div class="services-block">
            <i class="ion-ios-browsers-outline"></i>
            <span>SHORT FILM</span>
            <p class="separator">Saya pernah mengikuti perlombaan Video Pendek yang diselenggarakan oleh Forkom Bidikmisi Undiksha meraih Juara 3.</p>
          </div>
          <div class="services-block">
            <i class="ion-ios-color-wand-outline"></i>
            <span>DANCE</span>
            <p class="separator">Saya memiliki kemampuan menari baik tradisional maupun modern dan pernah mengikuti perlombaan dance dengan meraih juara harapan 3.</p>
          </div>
          <div class="services-block">
            <i class="ion-ios-analytics-outline"></i>
            <span>ACCOUNTANCY</span>
            <p class="separator">Saya memiliki kemampuan dibidang akutansi.</p>
          </div>
          <div class="services-block">
            <i class="ion-ios-camera-outline"></i>
            <span>PHOTOGRAPHY</span>
            <p class="separator">Ahli dibidang fotografi dan videografi serta pernah mengikuti kompetisi video pendek.</p>
          </div>
        </div>
    </div>
  </div>
  <!-- akhir skill -->

  <!-- journal -->
  <div id="journal" class="text-left paddsection">
    <div class="container">
      <div class="section-title text-center">
        <h2>Pengalaman</h2>
      </div>
    </div>
    <div class="container">
      <div class="journal-block">
        <div class="row">
          <div class="col-lg-4 col-md-6">
            <div class="journal-info">
              <a href="/pengalaman1"><img src="images/blog1.jpeg" class="img-responsive" alt="gambar blog"></a>
              <div class="journal-txt">
                <h4><a href="/pengalaman1">KOOR SUB BIDANG TARI HMJ TI 2020/2021</a></h4>
                <p class="separator">Saya pernah menjadi koor subbidang tari di HMJ TI 2020/2021.
                </p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="journal-info">
              <a href="/pengalaman2"><img src="images/blog2.jpeg" class="img-responsive" alt="gambar blog"></a>
              <div class="journal-txt">
                <h4><a href="/pengalaman2">KOOR BIDANG 2 HMJ TI 2021/2022</a></h4>
                <p class="separator">Saya saat ini menjabat menjadi koor bidang 2 HMJ TI pada tahun jabatan 2021/2022 yang mengurusi terkait minat dan bakat Mahasiswa Jurusan TI.
                </p>
              </div>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="journal-info">
              <a href="/pengalaman3"><img src="images/blog3.png" class="img-responsive" alt="gambar blog"></a>
              <div class="journal-txt">
                <h4><a href="/pengalaman3">LOMBA VIDEO PENDEK</a></h4>
                <p class="separator">Saya pernah mengikuti perlombaan Video Pendek yang diselenggarakan oleh Forkom Bidikmisi Undiksha dengan meraih juara 3.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
  <!-- journal -->

  <!-- Awal Galery -->
  <div id="portfolio" class="text-center paddsection">

    <div class="container">
      <div class="section-title text-center">
        <h2>Galery</h2>
      </div>
    </div>

    <div class="container">
      <div class="row">
        <div class="col-md-12">

          <div class="portfolio-list">

            <ul class="nav list-unstyled" id="portfolio-flters">
              <li class="filter filter-active" data-filter=".semua">semua</li>
              <li class="filter" data-filter=".organisasi">organisasi</li>
              <li class="filter" data-filter=".kesenian">kesenian</li>
              <li class="filter" data-filter=".kampus">kampus</li>
            </ul>

          </div>

          <div class="portfolio-container">

            <div class="col-lg-4 col-md-6 portfolio-thumbnail semua organisasi kampus">
              <a class="popup-img" href="images/portfolio/1.jpeg">
                <img src="images/portfolio/1.jpeg" alt="img">
              </a>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-thumbnail semua organisasi kampus">
              <a class="popup-img" href="images/portfolio/2.jpeg">
                <img src="images/portfolio/2.jpeg"  alt="img">
              </a>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-thumbnail semua organisasi kampus">
              <a class="popup-img" href="images/portfolio/3.jpeg">
                <img src="images/portfolio/3.jpeg" alt="img">
              </a>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-thumbnail semua kesenian">
              <a class="popup-img" href="images/portfolio/4.jpeg">
                <img src="images/portfolio/4.jpeg" alt="img">
              </a>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-thumbnail semua kesenian">
              <a class="popup-img" href="images/portfolio/5.jpeg">
                <img src="images/portfolio/5.jpeg" alt="img">
              </a>
            </div>

            <div class="col-lg-4 col-md-6 portfolio-thumbnail semua kampus">
              <a class="popup-img" href="images/portfolio/6.jpeg">
                <img src="images/portfolio/6.jpeg" alt="img">
              </a>
            </div>
            <div class="col-lg-4 col-md-6 portfolio-thumbnail semua kampus">
              <a class="popup-img" href="images/portfolio/7.jpeg">
                <img src="images/portfolio/7.jpeg" alt="img">
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
  <!-- Akhir Galery -->

  <!-- footer -->
  @extends('layout.footer')
<!-- footer -->

  <!-- JavaScript Libraries -->
  <script src="lib/jquery/jquery.min.js"></script>
  <script src="lib/jquery/jquery-migrate.min.js"></script>
  <script src="lib/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="lib/typed/typed.js"></script>
  <script src="lib/owlcarousel/owl.carousel.min.js"></script>
  <script src="lib/magnific-popup/magnific-popup.min.js"></script>
  <script src="lib/isotope/isotope.pkgd.min.js"></script>

  <!-- Contact Form JavaScript File -->
  <script>
    const scriptURL = 'https://script.google.com/macros/s/AKfycbwy1xx9lt0lifD1ermmM7wn8V5A6jX44P994dp8tNMe7uj2qLK-lLJSOWF9jQ6WLPNN/exec'
    const form = document.forms['biodata-contact-form']
  
    form.addEventListener('submit', e => {
      e.preventDefault()
      fetch(scriptURL, { method: 'POST', body: new FormData(form)})
        .then(response => console.log('Success!', response))
        .catch(error => console.error('Error!', error.message))
    })
  </script>

  <!-- Template Main Javascript File -->
  <script src="js/main.js"></script>

</body>

</html>
