<--! Footer -->
<div id="footer" class="text-center">
    <div class="socials-media text-center">
      <ul class="list-unstyled">
        <li><a href="https://www.facebook.com/profile.php?id=100006829174063" target="_blank"><i class="ion-social-facebook"></i></a></li>
        <li><a href="https://api.whatsapp.com/send?phone=6283102900652"><i class="ion-social-whatsapp" target="_blank"></i></a></li>
        <li><a href="https://www.instagram.com/indahprmt_13/" target="_blank"><i class="ion-social-instagram"></i></a></li>
      </ul>
    </div>
    <p>&copy; By Indah Permata</p>
</div>
