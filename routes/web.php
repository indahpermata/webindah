<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[App\Http\Controllers\HomeController::class,'index']);

Route::get('/pengalaman1',[App\Http\Controllers\Pengalaman1Controller::class,'index']);

Route::get('/pengalaman2',[App\Http\Controllers\Pengalaman2Controller::class,'index']);
Route::get('/pengalaman3',[App\Http\Controllers\Pengalaman3Controller::class,'index']);
